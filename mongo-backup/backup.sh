#!/bin/sh
mongodump -h mongo -u ${ME_CONFIG_MONGODB_ADMINUSERNAME} -p ${ME_CONFIG_MONGODB_ADMINPASSWORD} -o /backup/$(date +%y%m%d)/$(date +%s)
