const CronJob = require("cron").CronJob;
const shell = require("shelljs");

const str = `${process.env.SECONDS || "*"} ${process.env.MINUTES ||
	"*"} ${process.env.HOURS || "*"} ${process.env.DAY_OF_MONTH || "*"} ${process
	.env.MONTHS || "*"} ${process.env.DAY_OF_WEEK || "*"}`;

console.log(str);
new CronJob(str, function() {
	shell.exec("./backup.sh");
}).start();
